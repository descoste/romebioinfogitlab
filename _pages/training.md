---
title: "EMBL Rome Bioinformatics Services - Training"
layout: gridlay
excerpt: "EMBL Rome Bioinformatics Services - Training"
sitemap: false
permalink: /training/
---


# Training

<img src="{{ site.url }}{{ site.baseurl }}/images/slider7001400/20190617_RomeBI_1400x700px4.jpg" alt="Slide 4" height="350" width="700"/>

These trainings are provided in collaboration with [Bio-IT](https://bio-it.embl.de/). Please email nicolas.descostes@embl.it to register.
<br/>
<br/>


| Module  |  Requirements &nbsp;&nbsp; | Date  |  Teacher | Link |
|----------------------------------------------------------|--------------------------------------------------------|----------------------|-------------------|-------------------------------------------------|
||||||
| Intro to programming with R                       | None                                                   | 20 June  2019    | N. Descostes | [Here](https://descostesn.github.io/RIntroProgBio/) |
||||||
| ChIP-Seq with Galaxy            | None                                                   | 23 Sept 2019          | N. Descostes      |                                                 |
||||||
| RNA-Seq with Galaxy      | None                                                   | 31 Oct. 2019      | N. Descostes |                                                 |
||||||
| Unix command line                        | None                                                   | 25-26 Nov  2019  &nbsp;&nbsp;&nbsp;   | N. Descostes | [Here](https://git.embl.de/grp-bio-it/linuxcommandline) |
||||||
| Intro to HPC | Unix class                      | 18 dec 2019       | N. Descostes                | [Here](https://git.embl.de/grp-bio-it/embl_hpc)        |
||||||
| ChIP-Seq data processing with HPC  &nbsp;&nbsp;&nbsp;    | Unix/HPC | 27 Jan 2020 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  | N. Descostes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|                                                 |
||||||
| RNA-Seq data processing with HPC         | Unix/HPC | 24 Feb  2020 | N. Descostes |                                                 |
||||||
| Intro to programming with R                       | None                                                   | 23 March  2020    | N. Descostes |                                                 |
||||||
| Data Visualization with R                | Intro to R                     | 27 Apr 2020     | N. Descostes |                                                 |
||||||
| Intermediate R with Tidyverse                            | Intro to R                     | 25 May 2020       | N. Descostes |                                                 |
||||||
| Intro to git                                      | Unix                      | 29 June 2020      | N. Descostes |                                                 |


<br/>
<br/>

## Previous workshops

   + October 2018: Intro to programming with R
   + March 2019: Intro to Unix command line
   + April 2019: Intro to HPC usage
   + May 2019: Intro to Python (by Gregor Moenke)


{% assign number_printed = 0 %}
{% for publi in site.data.publist %}

{% assign even_odd = number_printed | modulo: 2 %}
{% if publi.highlight == 1 %}

{% if even_odd == 0 %}
<div class="row">
{% endif %}

<div class="col-sm-6 clearfix">
 <div class="well">
  <pubtit>{{ publi.title }}</pubtit>
  <img src="{{ site.url }}{{ site.baseurl }}/images/pubpic/{{ publi.image }}" class="img-responsive" width="33%" style="float: left" />
  <p>{{ publi.description }}</p>
  <p><em>{{ publi.authors }}</em></p>
  <p><strong><a href="{{ publi.link.url }}">{{ publi.link.display }}</a></strong></p>
  <p class="text-danger"><strong> {{ publi.news1 }}</strong></p>
  <p> {{ publi.news2 }}</p>
 </div>
</div>

{% assign number_printed = number_printed | plus: 1 %}

{% if even_odd == 1 %}
</div>
{% endif %}

{% endif %}
{% endfor %}

{% assign even_odd = number_printed | modulo: 2 %}
{% if even_odd == 1 %}
</div>
{% endif %}

<p> &nbsp; </p>




